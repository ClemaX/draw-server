require('rootpath')();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('util/jwt');
const errorHandler = require('util/error-handler');
const game = require('./game');

const app = express();

const PORT = 8080;

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth
app.use(jwt());

// api routes
app.use('/users', require('./users/users.controller'));

// error handler
app.use(errorHandler);

// start server
const server = app.listen(PORT, () => {
    console.log('Listening on port ' + PORT);
});

// start game-server
game(server);