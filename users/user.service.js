const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('util/db');
const User = db.User;

// read config
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

module.exports = {
    authenticate,
    getAll,
    getById,
    sendRequest,
    getRequests,
    acceptRequest,
    removeFriend,
    create,
    update,
    delete: _delete 
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        // eslint-disable-next-line no-unused-vars
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        return { ...userWithoutHash, token };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function sendRequest(id, idFriend) {
    if(id === idFriend) throw 'You can\'t add yourself as a friend';

    // validate
    if(!idFriend.match(/^[0-9a-fA-F]{24}$/)) {
        throw 'Invalid id';
    }

    const userTo = await getById(idFriend);

    // check if user exists // TODO: refactoring
    if(!userTo){
        throw 'Invalid user';
    }

    if(userTo.requests.findIndex(_id => _id === id) > -1) {
        throw 'The request has already been sent';
    } else if (userTo.friends.findIndex(_id => _id === id) > -1) {
        throw 'You are already friends';
    }

    userTo.requests.push(id);
    // save user
    await userTo.save();
}

async function getRequests(id) {
    const user = await getById(id);
    return user.requests;
}

async function acceptRequest(id, idFriend) { // TODO: rename functions
    var isRequested;

    // validate
    if(!idFriend.match(/^[0-9a-fA-F]{24}$/)) {
        throw 'Invalid id';
    }

    await User.findOne({_id: id, requests: idFriend}, function(err, user){
        isRequested = user !== null;
    });

    if(!isRequested){
        throw 'This request does not exist';
    }

    // remove friend from requests and add to friends
    await User.updateOne({_id: id}, {$pullAll: {requests: [idFriend]}, $addToSet: {friends: [idFriend]}});

    // add user to friend's friends
    await User.updateOne({_id: idFriend}, {$pullAll: {requests: [id]}, $addToSet: {friends: [id]}});
}

async function removeFriend(id, idFriend) {
    var isFriend;

    // validate
    if(!idFriend.match(/^[0-9a-fA-F]{24}$/)) {
        throw 'Invalid id';
    }

    await User.findOne({_id: id, friends: idFriend}, function(err, user){
        isFriend = user !== null;
    });

    if(!isFriend) {
        throw 'You are not friends with this user';
    }
    // remove friend from user's friendlist
    await User.updateOne({_id: id}, {$pullAll: {friends: [idFriend]}});
    // remove user from friends's friendlist
    await User.updateOne({_id: idFriend}, {$pullAll: {friends: [id]}});
}

async function create(userParam) {
    // validate
    if(await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if(userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if(!user) throw 'User not found';
    if(user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password
    if(userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy properties
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findOneAndDelete({_id: id});
}