const express = require('express');
const router = express.Router();
const userService = require('./user.service');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll); // TODO: permissions
router.get('/current', getCurrent);
router.get('/:id', getById);
router.get('/friends/requests', getRequests);
router.post('/friends/request/:id', sendRequest);
router.post('/friends/accept/:id', acceptRequest);
router.delete('/friends/:id', removeFriend);
router.put('/current', update);
router.delete('/current', _delete);

module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(_, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function sendRequest(req, res, next) {
    userService.sendRequest(req.user.sub, req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getRequests(req, res, next) {
    userService.getRequests(req.user.sub)
        .then(requests => res.json(JSON.stringify(requests)))
        .catch(err => next(err));
}

function acceptRequest(req, res, next) {
    userService.acceptRequest(req.user.sub, req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function removeFriend(req, res, next) {
    userService.removeFriend(req.user.sub, req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.user.sub, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.user.sub)
        .then(() => res.json({}))
        .catch(err => next(err));
}