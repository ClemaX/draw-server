const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    createdDate: { type: Date, default: Date.now },
    friends: [String],
    requests: [String] // TODO: remove requests, manage only through one array
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);