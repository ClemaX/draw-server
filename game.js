const redis = require('socket.io-redis');
const socketJwt = require('util/socket-jwt');
const shortid = require('shortid');

const socket = require('socket.io');

module.exports = game;

var io;
// var connectedClients = [];
var searchingClients = {}; // sub : client
var rooms = {}; // roomId : sockets{ sub: client }

function game(server) {
    io = socket(server);

    io.adapter(redis({
        host: 'localhost',
        port: 6379
    }));
    
    io.use(socketJwt);
    
    io.sockets.on('connection', (socket) => {
        socket.emit('authenticated');
        console.debug('[' + socket.decoded.sub + '] Authenticated');
    
        socket.on('disconnect', () => {
            delete searchingClients[socket.decoded.sub];
            console.debug('[' + socket.decoded.sub + '] Disconnected');
        });
    
        socket.on('createRoom', () => {
            const roomId = shortid.generate();
            var clients = {};
            clients[socket.decoded.sub] = socket;
            rooms[roomId] = clients;
            // socket.emit('drawer', {id: socket.decoded.sub}); // debug
            socket.join(roomId);
            console.debug('[' + socket.decoded.sub + ']' + ' -> ' + roomId);
            startGame(roomId);
        });
    
        socket.on('joinRoom', (data) => {
            var roomId = data.id;
            if (roomId in rooms) {
                socket.join(roomId);
                rooms[roomId][socket.decoded.sub] = socket;
                console.debug('[' + socket.decoded.sub + ']' + ' -> ' + roomId);
            } else {
                socket.emit('errorRoom'); // there is no such room
            }
        });
    
        socket.on('searchRoom', () => {
            // add client to the queue
            if (!(socket.decoded.sub in searchingClients)) {
                searchingClients[socket.decoded.sub] = socket;
            }
    
            console.debug('[' + socket.decoded.sub + ']' + ' Searching room (' + Object.keys(searchingClients).length + '/4)');
            const roomId = shortid.generate();
    
            if (Object.keys(searchingClients).length >= 4) { // when we have at least 4 concurrent players
                Object.keys(searchingClients).forEach(key => {
                    searchingClients[key].join(roomId);
                    console.debug('[' + searchingClients[key].decoded.sub + ']' + ' -> ' + roomId);
                });
    
                io.to(roomId).emit('room', {id: roomId}); // broadcast roomId to notify of join
    
                rooms[roomId] = searchingClients;
                startGame(roomId);
    
                searchingClients = {};
            }
        });
    });
    
}


function startGame(roomId) {
    var clients = rooms[roomId];
    var keys = Object.keys(clients);
    keys.forEach((key) => {
        clients[key].on('disconnect', () => {
            const userId = clients[key].decoded.sub;
            console.debug('[' + userId + '] Disconnected');
            io.to(roomId).emit('clientDisconnected', {id: clients[key].decoded.sub});
            delete rooms[roomId][userId];
        });
    });

    var drawerKey = keys[getRandomInt(0, keys.length - 1)];
    var drawer = clients[drawerKey];

    drawer.on('moveTo', (data) => {
        io.to(roomId).emit('moveTo', data);
        console.log('[' + roomId + '] moveTo: ' + data.x + ', ' + data.y);
    });

    drawer.on('lineTo', (data) => {
        io.to(roomId).emit('lineTo', data);
        console.log('[' + roomId + '] lineTo: ' + data.x + ', ' + data.y);
    });

    io.to(roomId).emit('drawer', { id: drawer.decoded.sub });

    console.debug('[' + drawer.decoded.sub + '][' + roomId + '] -> Drawer');
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}