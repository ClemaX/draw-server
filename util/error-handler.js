module.exports = errorHandler;

// eslint-disable-next-line no-unused-vars
function errorHandler(err, req, res, next) {
    if(typeof(err) === 'string') {
        // custom error
        return res.status(400).json({ message: err });
    }
    // TODO: Organize errors and send correct error codes

    if(err.name === 'ValidationError') {
        // mongoose error
        return res.status(400).json({ message: err.message });
    }

    if(err.name === 'UnauthorizedError') {
        // authentication error
        return res.status(401).json({ message: 'Invalid Token'});
    }

    // default
    return res.status(500).json({ message: err.message });
}