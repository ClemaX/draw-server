const fs = require('fs');
const expressJwt = require('express-jwt');
const userService = require('users/user.service');

module.exports = jwt;

function jwt() {
    // read config
    const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
    const secret = config.secret;
    
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes
            '/users/authenticate',
            '/users/register'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if(!user) {
        return done(null, true);
    }

    done();
}