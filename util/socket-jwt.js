const fs = require('fs');
const jwt = require('jsonwebtoken');

module.exports = socketJwt;

// TODO: Check if user exists

function socketJwt(socket, next) {
    const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
    const secret = config.secret;

    if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token, secret, function (err, decoded) {
            if (err) {
                console.log('Authentication error');
                return next(new Error('UnauthorizedError'));
            }
            socket.decoded = decoded;
            next();
        });
    } else {
        console.log('Authentication error');
        next(new Error('UnauthorizedError'));
    }
}