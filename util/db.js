const fs = require('fs');
const mongoose = require('mongoose');

// read config
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true }).catch(err => {
    if (err.name == 'MongoNetworkError')
        console.error('Could not reach ' + config.connectionString);
    process.exit(1);
});
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model')
};